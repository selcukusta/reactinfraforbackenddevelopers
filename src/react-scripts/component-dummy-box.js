import React from 'react'
export default class DummyBox extends React.Component {
  render() {
    let listItems = [];
    let data = this.props.data;
    data.forEach(function (item) {
      listItems.push(<ListItem key={item.id} data={item} />)
    }.bind(this));

    return (
      <div className="DummyBox">
        <p>Hello, world!This is another DummyBox!</p>
        <ol>
          {listItems}
        </ol>
      </div>
    );
  }
};

class ListItem extends React.Component {
  render() {
    return (
      <li>{this.props.data.name} - {this.props.data.popularity}</li>
    );
  }
}