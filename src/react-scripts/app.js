import React from 'react';
import ReactDOM from 'react-dom';
import {data} from './data-file';
import DummyBox from './component-dummy-box';
ReactDOM.render(
  <DummyBox data={data} />,
  document.getElementById('another-content')
);