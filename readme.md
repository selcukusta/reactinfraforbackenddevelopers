1. Package.json dosyasını npm init ile olustur...
2. npm install --save-dev babel-preset-es2015 babel-preset-react babelify browserify gulp gulp-uglify vinyl-buffer vinyl-source-stream    
3. npm install --save react react-dom
4. Task'ların olusturulmasi icin gulpfile.js dosyasi yaratilir.
5. CTRL + Shift + B ile build calistirilir ancak herhangi bir task olusturulmadigi icin hata verecektir. Sonrasinda cikan uyaridan "yeni bir task olustur" secilip "Gulp" kullanilacagi belirtilirse otomatik tasks.json dosyasi olusacaktir.
6. CTRL + Shift + P sonrasinda acilacak kucuk konsol ekranına "Run Task" yazılır. Sonrasinda ise sizden task ismini isteyecektir. Bu isim icin de "CreateProductionVendorJs" ifadesini kullanabilirsiniz (bu biraz once olusturdugumuz task'in ismi!)
7. Eger "OUTPUT" ekraninda task'in basarili bir bicimde olusturuldugu yaziyor ise vendor.js sorunsuz yaratilmis demektir.
8. index.html dosyasini olusturalim ve script'i refere edelim. Eger Chrome Console ekraninda bir hata yoksa react-vendor.min.js dosyasinda da sorun yok demektir.
9. Sadece react ozelinde degil, kullandiginiz javascript ve css dosyalarini da yine gulp tasklari araciligiyla birlestirebilir ve minify edebilirsiniz.